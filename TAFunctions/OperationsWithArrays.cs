using System;

namespace TAFunctions
{
    public enum SortOrder { Ascending, Descending};
    public static class OperationsWithArrays
    {
        public static void SortArray(int[] array, SortOrder order)
        {
            Array.Sort(array);
            if (order == SortOrder.Descending)
            {
               Array.Reverse(array);
            }
        }

        public static bool IsSorted(int[] array, SortOrder order)
        {
            Array.Sort(array);
            if (order == SortOrder.Descending)
            {
               Array.Reverse(array);
            }
            if (order == SortOrder.Ascending) {
                return true;
            } else
            {
                return false;
            }
        }
    }
}